---
title: Internet of Things
date: 2018-08-04 14:41:39 +0000

---
As part of my research into the latest trends happening on the internet, I have come across the latest ~~security vulnerability~~ feature, the Internet of Things. So how best to understand this new feature, than to build some of my own!

### Weather
![weather station](/images/posts/iot.jpg)

#### Hardware
* [Aercus Instruments Wireless Weather Station WeatherSleuth](https://www.amazon.co.uk/Wireless-Weather-Station-WeatherSleuth%C2%AE-Professional/dp/B00ZVFZUS6/ref=sr_1_3?ie=UTF8&qid=1511662161&sr=8-3&keywords=aercus+instruments)

#### Services
* [wunderground](https://www.wunderground.com/personal-weather-station/dashboard?ID=ILLANTWI4)

#### Interact
[frontend &raquo;]() - Coming soon

### NTP
#### Hardware
* [Raspberry Pi 3](https://www.amazon.co.uk/gp/product/B01CD5VC92/ref=oh_aui_detailpage_o05_s00?ie=UTF8&psc=1)
* [adafruit Ultimate GPS](https://thepihut.com/products/adafruit-ultimate-gps-logger-shield-includes-gps-module)

#### Software
* [ntp](https://ntp.org)
* [gpsd](http://www.catb.org/gpsd/)
* [ngrok](http://ngrok.com)
* [ntp-viewer](https://github.com/matthewgall/ntp-viewer)

#### Interact
[frontend &raquo;](https://ntp.londinium.ninja)  
[.well-known/time &raquo;](https://ntp.londinium.ninja/.well-known/time)

### Flight Tracking
#### Hardware
* [Raspberry Pi 3](https://www.amazon.co.uk/gp/product/B01CD5VC92/ref=oh_aui_detailpage_o05_s00?ie=UTF8&psc=1)
* [NooElec NESDR SMArt](https://www.amazon.co.uk/gp/product/B01GDN1T4S/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)
* [FlightAware 1090MHz ADS-B Antenna](https://www.amazon.co.uk/gp/product/B00WZL6WPO/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)

#### Software
* dump1090-mutability
* modesmixer2
* pfclient
* fr24feed
* piaware
* [ngrok](http://ngrok.com)
* [cloudflare-warp](https://warp.cloudflare.com)

#### Services
* [flightradar24](https://www.flightradar24.com)
* [FlightAware](http://uk.flightaware.com/adsb/stats/user/matthewgall)
* [PlaneFinder](https://planefinder.net/sharing/receiver/94445)

#### Interact
[frontend &raquo;](https://adsb.londinium.ninja)