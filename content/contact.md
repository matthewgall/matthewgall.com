---
title: Contact
date: 2018-08-04 14:36:28 +0000
image: images/pages/contact.jpg

---
Contacting me is pretty easy, I'm everywhere! Take your pick from one of the services below!

## For the old fashioned

[hey@matthewgall.com](mailto:hey@matthewgall.com)

## For the hip

[Twitter](https://twitter.com/matthewgall) - @matthewgall  
[Facebook](https://facebook.com/thematthewgall) - @thematthewgall
[Mastodon](https://mastodon.at/@matthew) - @matthew

## For my colleagues

[LinkedIn](https://linkedin.com/in/thematthewgall) - @thematthewgall

## For the paranoid

[Telegram](telegram://matthewgall) - @matthewgall  
[Keybase](https://keybase.io/matthewgall) - @matthewgall  
[Wire](https://wire.com) - @thematthewgall

### and technical

[PGP](https://keybase.io/matthewgall/pgp_keys.asc)

## For the unique

[XMPP/Jabber](xmpp://matthewgall@conversations.im) - matthewgall@conversations.im