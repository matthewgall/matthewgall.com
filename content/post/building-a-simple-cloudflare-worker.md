+++
title = "Building a simple Cloudflare Worker"
date = "2018-08-04T20:38:27+00:00"
draft = true
image = "images/posts/workers.jpg"
+++

The future of the internet appears to be serverless. From complex binaries, to simple javascript, making minor edits or majorly dynamic websites without a server are now not beyond the realms of the common developer! In this guide, we're going to explore static websites, using Cloudflare Workers!

## Before we begin...

_This is post is written from my perspective as an end user and not as part of my employment with Cloudflare Inc. The views and opinions expressed are mine, and mine alone and do not represent the views of my employer, or any third party_