---
title: "Welcome... to... well, me!"
date: 2017-11-26T15:27:54+01:00
image: "images/posts/hello.jpg"
---

A dodgy upgrade of Ghost has lost my website, and as such, I thought it was time to start again.

Stay tuned for more from me!